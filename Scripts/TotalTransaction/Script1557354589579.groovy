import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.212.172.223:8083/')

WebUI.setText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/input_Username_username'), 'admin')

WebUI.setEncryptedText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/input_Password_password'), 
    'RAIVpflpDOg=')

WebUI.click(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/a_Total Transaction'))

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_1'), '1')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_2'), '2')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_3'), '3')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_4'), '4')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_5'), '5')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_6'), '6')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_Garden Papaya'), 'Garden, Papaya')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_Banana Garden Banana Rambutan'), 
    'Banana, Garden, Banana, Rambutan')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_Banana Papaya'), 'Banana, Papaya')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_Banana'), 'Banana')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_Banana'), 'Banana')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_Banana'), 'Banana')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_20120 THB'), '20,120 THB')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_60570 THB'), '60,570 THB')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_162 THB'), '162 THB')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_150 THB'), '150 THB')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_150 THB'), '150 THB')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/td_150 THB'), '150 THB')

WebUI.verifyElementText(findTestObject('Object Repository/TotalTransaction/Page_ProjectBackend/p_Total price  81302 THB'), 
    'Total price: 81,302 THB')

WebUI.closeBrowser()

