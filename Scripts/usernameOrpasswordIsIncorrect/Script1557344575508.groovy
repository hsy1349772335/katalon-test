import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.212.172.223:8083/')

WebUI.click(findTestObject('Object Repository/ErrorMessageIncorrect/Page_ProjectBackend/h2_Login'))

WebUI.click(findTestObject('Object Repository/ErrorMessageIncorrect/Page_ProjectBackend/label_Username'))

WebUI.setText(findTestObject('Object Repository/ErrorMessageIncorrect/Page_ProjectBackend/input_Username_username'), '564545')

WebUI.click(findTestObject('Object Repository/ErrorMessageIncorrect/Page_ProjectBackend/label_Password'))

WebUI.setEncryptedText(findTestObject('Object Repository/ErrorMessageIncorrect/Page_ProjectBackend/input_Password_password'), 
    '2h/+wqBT6Rc=')

WebUI.click(findTestObject('Object Repository/ErrorMessageIncorrect/Page_ProjectBackend/button_Login'))

WebUI.verifyElementText(findTestObject('Object Repository/ErrorMessageIncorrect/Page_ProjectBackend/label_Usernamepassword is incorrect'), 
    'Username/password is incorrect')

WebUI.closeBrowser()

